package com.epam;

import com.epam.browser.ChromeBrowser;
import com.epam.factory.PagesFactory;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.Request;
import org.junit.runner.Result;

public class UnitTest {

    @BeforeClass
    public static void setUp() throws Exception{

        System.setProperty("webdriver.chrome.driver", "C:/Development/chrome_driver/chromedriver.exe");
        ChromeBrowser.openDefaultPage();
        PagesFactory.getRegistrationPage().openPage();
    }

    @AfterClass
    public static void cleanUp() {
        //ChromeBrowser.closeBrowser();
    }

    @Test
    public void canOpenRegistrationPage(){
        Assert.assertTrue(PagesFactory.getRegistrationPage().isAt());
    }

    @Test
    public void emptyUserNameFields() {
        Assert.assertTrue(PagesFactory.getRegistrationPage().isEmptyUserNameFields());
    }

    @Test
    public void defaultValueOfUserBirthday() {
        Assert.assertTrue(PagesFactory.getRegistrationPage().isDefaultValueOfUserBirthday());
    }

    @Test
    public void defaultSelectGenderFields(){
        Assert.assertFalse(PagesFactory.getRegistrationPage().isDefaultSelectGenderFields());
    }

    @Test
    public void defaultSelectCountryValue(){
        Assert.assertFalse(PagesFactory.getRegistrationPage().isDefaultSelectCountryValue());
    }

    @Test
    public void changeSelectCountryValue(){
        Assert.assertTrue(PagesFactory.getRegistrationPage().changeSelectCountryValue());
    }

    @Test
    public void defaultAlert(){
        Assert.assertTrue(PagesFactory.getRegistrationPage().alertAfterDefaultFormSubmit());
    }

    @Test
    public void textAreaFields(){
        Assert.assertTrue(PagesFactory.getRegistrationPage().isTextAreaFields());
    }


}
